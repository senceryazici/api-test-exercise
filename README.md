# API Test Exercise
Basic and simple example of a test API over TCP/IP, hosted on `localhost:1864`

## Usage
- Run [APIServer.py](APIServer.py) to start the server
- Run [test.py](test.py) to start the client example.

## License
You can do anything with this repository. [8 Agu 2020]

## Author
Sencer Yazici, [senceryazici@gmail.com](mailto:senceryazici@gmail.com)